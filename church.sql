-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Dec 16, 2017 at 06:25 PM
-- Server version: 10.1.22-MariaDB
-- PHP Version: 7.0.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `church`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(50) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `username`, `password`) VALUES
(1, 'admin', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `doubletable`
--

CREATE TABLE `doubletable` (
  `quiz_id` int(10) NOT NULL,
  `userid` int(10) NOT NULL,
  `dd_flag` int(10) NOT NULL,
  `dd_wager` int(10) NOT NULL,
  `dj_flag` int(10) NOT NULL,
  `dj_wager` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `doubletable`
--

INSERT INTO `doubletable` (`quiz_id`, `userid`, `dd_flag`, `dd_wager`, `dj_flag`, `dj_wager`) VALUES
(1, 42, 0, 0, 0, 0),
(1, 43, 0, 0, 0, 0),
(1, 44, 0, 0, 0, 0),
(1, 45, 2, 500, 2, 1000),
(1, 46, 0, 0, 0, 0),
(1, 47, 0, 0, 0, 0),
(1, 48, 2, 800, 2, 1000),
(2, 49, 0, 0, 0, 0),
(2, 50, 0, 0, 0, 0),
(2, 51, 0, 0, 0, 0),
(2, 52, 0, 0, 0, 0),
(2, 53, 0, 0, 0, 0),
(1, 54, 0, 0, 0, 0),
(1, 55, 0, 0, 0, 0),
(1, 56, 0, 0, 0, 0),
(1, 57, 0, 0, 0, 0),
(1, 58, 0, 0, 0, 0),
(1, 59, 0, 0, 0, 0),
(1, 60, 0, 0, 0, 0),
(1, 61, 0, 0, 0, 0),
(1, 62, 0, 0, 0, 0),
(1, 63, 0, 0, 0, 0),
(1, 64, 0, 0, 0, 0),
(1, 65, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `email_verify`
--

CREATE TABLE `email_verify` (
  `id` int(50) NOT NULL,
  `user_id` int(50) NOT NULL,
  `token` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `email_verify`
--

INSERT INTO `email_verify` (`id`, `user_id`, `token`) VALUES
(1, 1, '59422893b038a'),
(2, 2, '59422966efbc5'),
(3, 3, '5942299021052'),
(4, 4, '5944c39540c39');

-- --------------------------------------------------------

--
-- Table structure for table `options`
--

CREATE TABLE `options` (
  `id` int(50) NOT NULL,
  `question_id` int(50) NOT NULL,
  `option_text` varchar(500) NOT NULL,
  `correct` int(1) NOT NULL,
  `option_text_ml` varchar(1000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `options`
--

INSERT INTO `options` (`id`, `question_id`, `option_text`, `correct`, `option_text_ml`) VALUES
(1, 1, 'options1 so large that you what ?????????????? Yes you heard it correctly lel', 0, 'നിങ്ങൾ വളരെ വലുതായിട്ടുള്ള ഓപ്ഷനുകൾ അതെ നിങ്ങൾ ശരിയായി കേൾക്കുന്നു'),
(2, 1, 'options2 so large that you what ?????????????? Yes you heard it correctly lel', 0, 'നിങ്ങൾ വളരെ വലുതായിട്ടുള്ള ഓപ്ഷനുകൾ അതെ നിങ്ങൾ ശരിയായി കേൾക്കുന്നു'),
(3, 1, 'options3 so large that you what ?????????????? Yes you heard it correctly lel', 1, 'നിങ്ങൾ വളരെ വലുതായിട്ടുള്ള ഓപ്ഷനുകൾ അതെ നിങ്ങൾ ശരിയായി കേൾക്കുന്നു'),
(4, 1, 'options4 so large that you what ?????????????? Yes you heard it correctly lel', 0, 'നിങ്ങൾ വളരെ വലുതായിട്ടുള്ള ഓപ്ഷനുകൾ അതെ നിങ്ങൾ ശരിയായി കേൾക്കുന്നു'),
(5, 2, 'qefijiokedjfioejdf', 0, 'ഹായ്, സുഖമാണോ'),
(6, 2, 'fuioqwehuofhuh', 0, 'ഹായ്, സുഖമാണോ'),
(7, 2, 'wujfhweujufhguo', 1, 'efuhwoghf ഹായ്, സുഖമാണോ'),
(8, 2, 'eofuihwesuofghoh', 0, 'ഹായ്, സുഖമാണോ'),
(9, 3, 'qewfihei', 0, 'ഹായ്, സുഖമാണോ'),
(10, 3, 'wefhjewfh', 1, 'ഹായ്, സുഖമാണോ'),
(11, 3, 'wfidwehfuioeh', 0, 'ഹായ്, സുഖമാണോഹായ്, സുഖമാണോ'),
(12, 3, 'weifhjewfghj', 0, 'ഹായ്, സുഖമാണോ'),
(13, 4, 'eifjwe', 0, 'ഹായ്, സുഖമാണോ'),
(14, 4, 'efije', 0, 'ഹായ്, സുഖമാണോ'),
(15, 4, 'qwefihjqeifhj', 1, 'ഹായ്, സുഖമാണോ'),
(16, 4, 'wjifqfjijf', 0, 'ഹായ്, സുഖമാണോ'),
(17, 5, 'wfojqjf', 1, 'ഹായ്, സുഖമാണോ'),
(18, 5, 'dwd', 0, 'ഹായ്, സുഖമാണോ'),
(19, 5, 'qwef', 0, 'ef'),
(20, 5, 'qwedfwef', 0, 'qawefqwef'),
(21, 6, 'dsvsdv', 0, 'ഹായ്, സുഖമാണോ'),
(22, 6, 'wdwf', 1, 'ഹായ്, സുഖമാണോ'),
(23, 6, 'wdadwaed', 0, 'ഹായ്, സുഖമാണോ'),
(24, 6, 'awdwad', 0, 'ഹായ്, സുഖമാണോ'),
(25, 7, 'awdawdwad', 1, 'ഹായ്, സുഖമാണോ'),
(26, 7, 'wadwad', 0, 'ഹായ്, സുഖമാണോ'),
(27, 7, 'awDad', 0, 'ഹായ്, സുഖമാണോ'),
(28, 7, 'wdawdawd', 0, 'ഹായ്, സുഖമാണോ'),
(29, 8, 'wdwad', 0, 'ഹായ്, സുഖമാണോ'),
(30, 8, 'wdewqadaw', 1, 'ഹായ്, സുഖമാണോ'),
(31, 8, 'wadawdwad', 0, 'ഹായ്, സുഖമാണോ'),
(32, 8, 'awdawd', 0, 'ഹായ്, സുഖമാണോ'),
(33, 9, 'wdwad', 0, 'efef'),
(34, 9, 'easfesf', 0, 'aefaef'),
(35, 9, 'awefawef', 1, 'ഹായ്, സുഖമാണോ'),
(36, 9, 'awdawd', 0, 'ഹായ്, സുഖമാണോ'),
(37, 10, '4', 1, ''),
(38, 10, '5', 0, ''),
(39, 10, '6', 0, ''),
(40, 10, '1', 0, ''),
(41, 11, '10', 1, '10'),
(42, 11, '3', 0, '3'),
(43, 11, '2', 0, '2'),
(44, 11, '15', 0, '15'),
(45, 12, '12', 1, '12'),
(46, 12, '10', 0, '5'),
(47, 12, '11', 0, '11'),
(48, 12, '1', 0, '1'),
(49, 13, '30', 0, '30'),
(50, 13, '1', 1, '1'),
(51, 13, '20', 0, '20'),
(52, 13, '11', 0, '11'),
(53, 14, 'Aasim', 0, '500'),
(54, 14, 'a', 1, ''),
(55, 14, 'a', 0, ''),
(56, 14, 'a', 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `questions`
--

CREATE TABLE `questions` (
  `id` int(11) NOT NULL,
  `quiz_id` int(11) NOT NULL,
  `english` varchar(500) NOT NULL,
  `malyalam` varchar(500) CHARACTER SET utf8 NOT NULL,
  `points` int(50) NOT NULL,
  `custom_points` int(10) NOT NULL,
  `category` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `questions`
--

INSERT INTO `questions` (`id`, `quiz_id`, `english`, `malyalam`, `points`, `custom_points`, `category`) VALUES
(1, 1, 'I\'m writing a very long question to check whether it looks good in the question screen or not. Mostly, It will but you never know. There\'s also one more thing I need to write is the code of question.js', 'ചോദ്യം സ്ക്രീനിൽ നല്ലതാണോയെന്ന് പരിശോധിക്കാൻ ഞാൻ ഒരുപാട് ചോദ്യങ്ങളാണ് എഴുതുന്നത്. മിക്കവാറും, അത് നിങ്ങൾക്ക് ഒരിക്കലും അറിയാൻ കഴിയില്ല. ഞാൻ എഴുതേണ്ട ഒരു കാര്യം കൂടിതന്നെ question.js ന്റെ കോഡ് ആണ്', 100, 100, 1),
(2, 1, 'awdawediuj', 'ഹായ്, സുഖമാണോഹായ്, സുഖമാണോ wqrdfqwfq', 100, 100, 2),
(3, 1, 'dwdwiapdji', 'ഹായ്, സുഖമാണോ', 100, 300, 3),
(4, 1, 'wqwefjo', 'ഹായ്, സുഖമാണോ', 100, 300, 4),
(5, 2, 'qwef', 'ഹായ്, സുഖമാണോ', 100, 400, 1),
(6, 3, 'wadfawf', 'asedgvsr', 100, 0, 1),
(7, 3, 'wadawdawd', 'ഹായ്, സുഖമാണോ', 100, 0, 2),
(8, 3, 'awdwad', 'awdഹായ്, സുഖമാണോ', 100, 0, 3),
(9, 3, 'wadawd', 'awdwadഹായ്, സുഖമാണോ', 100, 0, 4),
(10, 2, 'What is 2+2 = ?', 'What is 2+2 = ?', 100, 200, 2),
(11, 1, '5+5', '5*5', 100, 300, 5),
(12, 1, '4*3', '4*3', 200, 400, 1),
(13, 1, '6-5', '6-5', 200, 400, 2),
(14, 2, 'Google', 'Google', 100, 1000, 3);

-- --------------------------------------------------------

--
-- Table structure for table `quiz`
--

CREATE TABLE `quiz` (
  `id` int(50) NOT NULL,
  `start_time` date NOT NULL,
  `end_time` date NOT NULL,
  `title` varchar(100) NOT NULL,
  `ongoing_flag` int(11) NOT NULL DEFAULT '0',
  `curr_chance` int(10) NOT NULL,
  `c1` varchar(100) NOT NULL,
  `c2` varchar(100) NOT NULL,
  `c3` varchar(100) NOT NULL,
  `c4` varchar(100) NOT NULL,
  `c5` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `quiz`
--

INSERT INTO `quiz` (`id`, `start_time`, `end_time`, `title`, `ongoing_flag`, `curr_chance`, `c1`, `c2`, `c3`, `c4`, `c5`) VALUES
(1, '2017-06-15', '2017-06-18', 'Tech', 1, 64, 'Category 1 Which has a long name', 'Category2 ', 'Category 3, Another long one', 'Category 5 short', 'sh'),
(2, '2017-11-09', '2017-12-23', 'Techdawd', 1, 51, 'Random', 'Tech', 'Religion', 'Science', 'Maths'),
(3, '2017-06-11', '2017-06-20', 'General', 0, 0, '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `quiz_answers`
--

CREATE TABLE `quiz_answers` (
  `id` int(50) NOT NULL,
  `user_id` int(50) NOT NULL,
  `questions_id` int(50) NOT NULL,
  `quiz_id` int(50) NOT NULL,
  `points` decimal(50,0) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `quiz_answers`
--

INSERT INTO `quiz_answers` (`id`, `user_id`, `questions_id`, `quiz_id`, `points`) VALUES
(187, 64, 12, 1, '400'),
(188, 64, 13, 1, '200'),
(189, 65, 13, 1, '0');

-- --------------------------------------------------------

--
-- Table structure for table `quiz_participants`
--

CREATE TABLE `quiz_participants` (
  `id` int(10) NOT NULL,
  `quiz_id` int(10) NOT NULL,
  `team_name` varchar(1000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `quiz_participants`
--

INSERT INTO `quiz_participants` (`id`, `quiz_id`, `team_name`) VALUES
(49, 2, 'a'),
(50, 2, 'b'),
(51, 2, 'c'),
(52, 2, 'd'),
(53, 2, 'e'),
(64, 1, 'aasim'),
(65, 1, 'aasim2');

-- --------------------------------------------------------

--
-- Table structure for table `reset_password`
--

CREATE TABLE `reset_password` (
  `id` int(50) NOT NULL,
  `user_id` int(50) NOT NULL,
  `token` varchar(100) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(100) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(100) NOT NULL,
  `email` varchar(80) NOT NULL,
  `contact` int(10) NOT NULL,
  `type` int(1) NOT NULL,
  `verified` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `email`, `contact`, `type`, `verified`) VALUES
(1, 'aasimkhan30', '$2y$12$9tzHUHwCLTg4SpZxHUXSz.KGgTmdSLEvgGA1d3KoohiC800W4tB.a', 'aasimkhan30@gmail.com', 0, 0, 1),
(2, 'aasimkhan301', '$2y$12$GTYiuj1mCOSeXBz61hIov.zrIGzaFY1iMrSDAzdflA57m313czNgy', 'aasim.khan30@gmail.com', 0, 0, 1),
(3, 'aasimkhan302', '$2y$12$a.dTT9h1kF90uGQUSsmuwelW5gUXOSgDvzagjikcZnHyFEx7zuLM6', 'aa.simkhan30@gmail.com', 0, 0, 1),
(4, 'aasimkhan303', '$2y$12$1ytXelSCays99ljrmLE4Ze2EQtTZANIL0k0DQ.ZngL6OvsSw9jYmG', 'aasimkhan3045@gmail.com', 0, 0, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `doubletable`
--
ALTER TABLE `doubletable`
  ADD PRIMARY KEY (`userid`);

--
-- Indexes for table `email_verify`
--
ALTER TABLE `email_verify`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `options`
--
ALTER TABLE `options`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `questions`
--
ALTER TABLE `questions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `quiz`
--
ALTER TABLE `quiz`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `quiz_answers`
--
ALTER TABLE `quiz_answers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `quiz_participants`
--
ALTER TABLE `quiz_participants`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reset_password`
--
ALTER TABLE `reset_password`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `email_verify`
--
ALTER TABLE `email_verify`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `options`
--
ALTER TABLE `options`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;
--
-- AUTO_INCREMENT for table `questions`
--
ALTER TABLE `questions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `quiz`
--
ALTER TABLE `quiz`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `quiz_answers`
--
ALTER TABLE `quiz_answers`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=190;
--
-- AUTO_INCREMENT for table `quiz_participants`
--
ALTER TABLE `quiz_participants`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=66;
--
-- AUTO_INCREMENT for table `reset_password`
--
ALTER TABLE `reset_password`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
