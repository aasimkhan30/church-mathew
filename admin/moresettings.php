<?php
/**
 * Created by PhpStorm.
 * User: aasimkhan30
 * Date: 10/6/17
 * Time: 10:10 PM
 */
session_start();
if (!isset($_SESSION["admin_loggedin"])) { // this means the user session is set
    header("Location:index.php");
}

?>
<!DOCTYPE html>
<html>
<head>
    <link href="css/style.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="css/uikit.css" type="text/css" rel="stylesheet" media="screen,projection"/>
    <link href="css/style.css" type="text/css" rel="stylesheet" media="screen,projection"/>
    <!--Import jQuery before materialize.js-->
    <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
    <script type="text/javascript" src="js/uikit.js"></script>
    <script type="text/javascript" src="js/uikit-icons.js"></script>
    <style type="text/css">
        .createquiz {
            background-color: #008CBA;
        }
    </style>
</head>

<body>
<div uk-sticky="media: 960" class="uk-navbar-container tm-navbar-container uk-sticky" style="">
    <div class="uk-container uk-container-expand">
        <nav class="uk-navbar-container " uk-navbar>
            <div class="uk-navbar-left">
                <a class="uk-navbar-item uk-logo" href="index.php">Church</a>
            </div>
            <div class="uk-navbar-right">
                <ul class="uk-navbar-nav">
                    <li><a href="moresettings.php" style="color:white; font-weight: 300">More Settings</a></li>
                </ul>
                <a href="apis/admin_logout.php" class=" uk-button uk-button-default navbar-button">Logout</a>
            </div>

        </nav>
    </div>
</div>
<div class="uk-container">
    <br>
    <h1>More Settings</h1>
    <hr>
    <p>Change Music File</p>
    <div class="uk-margin" uk-margin>
        <form id="uploadmusic">
            <div id="music_message"></div>
            <div uk-form-custom="target: true">
                <input type="file" name="file" id="file" required>
                <input class="uk-input uk-form-width-medium" type="text" placeholder="Select file" disabled>
            </div>
            <button class="uk-button uk-button-default">Submit</button>
            <div id="musicspinner" uk-spinner></div>
        </form>

    </div>

</div>
<script>

    $(document).ready(function (e) {
        $("#message").hide();
        $('#musicspinner').hide();
        $("#uploadmusic").on('submit', (function (e) {
            e.preventDefault();
            console.log("Clicked submit");
            $('#musicspinner').show();
            $('#music_message').hide();
            $.ajax({
                url: "apis/music_upload.php", // Url to which the request is send
                type: "POST",             // Type of request to be send, called as method
                data: new FormData(this), // Data sent to server, a set of key/value pairs (i.e. form fields and values)
                contentType: false,       // The content type used when sending data to the server.
                cache: false,             // To unable request pages to be cached
                processData: false,        // To send DOMDocument or non processed data file it is set to false
                success: function (data)   // A function to be called if request succeeds
                {
                    console.log("Success"+data);
                    $('#musicspinner').hide();
                    $('#music_message').show();
                    $('#music_message').html(data);
                },
            });
        }));
    });
</script>
</body>

</html>