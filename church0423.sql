-- phpMyAdmin SQL Dump
-- version 3.5.8.2
-- http://www.phpmyadmin.net
--
-- Host: sql301.byethost.com
-- Generation Time: Apr 23, 2018 at 02:54 PM
-- Server version: 5.6.35-81.0
-- PHP Version: 5.3.3

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `b10_21123313_church`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
  `id` int(50) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `username`, `password`) VALUES
(1, 'admin', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `doubletable`
--

CREATE TABLE IF NOT EXISTS `doubletable` (
  `quiz_id` int(10) NOT NULL,
  `userid` int(10) NOT NULL,
  `dd_flag` int(10) NOT NULL,
  `dd_wager` int(10) NOT NULL,
  `dj_flag` int(10) NOT NULL,
  `dj_wager` int(10) NOT NULL,
  PRIMARY KEY (`userid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `doubletable`
--

INSERT INTO `doubletable` (`quiz_id`, `userid`, `dd_flag`, `dd_wager`, `dj_flag`, `dj_wager`) VALUES
(8, 99, 0, 0, 2, 1000),
(6, 98, 0, 0, 0, 0),
(6, 97, 0, 0, 0, 0),
(6, 96, 0, 0, 0, 0),
(6, 95, 0, 0, 0, 0),
(8, 94, 0, 0, 0, 0),
(8, 93, 0, 0, 0, 0),
(8, 92, 0, 0, 0, 0),
(8, 91, 0, 0, 0, 0),
(8, 90, 0, 0, 0, 0),
(8, 89, 0, 0, 0, 0),
(8, 88, 0, 0, 0, 0),
(8, 87, 0, 0, 0, 0),
(6, 73, 0, 0, 0, 0),
(6, 72, 0, 0, 0, 0),
(8, 100, 0, 0, 0, 0),
(8, 101, 0, 0, 0, 0),
(8, 102, 0, 0, 0, 0),
(8, 103, 0, 0, 0, 0),
(8, 104, 0, 0, 0, 0),
(8, 105, 0, 0, 0, 0),
(8, 106, 0, 0, 0, 0),
(8, 107, 0, 0, 0, 0),
(8, 108, 0, 0, 0, 0),
(8, 109, 0, 0, 0, 0),
(8, 110, 0, 0, 0, 0),
(9, 111, 0, 0, 0, 0),
(9, 112, 0, 0, 0, 0),
(9, 113, 0, 0, 0, 0),
(9, 114, 0, 0, 0, 0),
(12, 115, 0, 0, 0, 0),
(12, 116, 0, 0, 0, 0),
(13, 117, 2, 500, 2, 500),
(13, 118, 2, 200, 2, 1300),
(13, 119, 2, 600, 2, 300),
(13, 120, 2, 300, 2, 250),
(13, 121, 2, 300, 2, 1050),
(14, 122, 0, 0, 0, 0),
(14, 123, 0, 0, 0, 0),
(14, 124, 0, 0, 0, 0),
(9, 125, 0, 0, 0, 0),
(9, 126, 0, 0, 0, 0),
(9, 127, 0, 0, 0, 0),
(9, 128, 0, 0, 0, 0),
(9, 129, 0, 0, 0, 0),
(12, 130, 2, 600, 0, 0),
(12, 131, 0, 0, 0, 0),
(12, 132, 0, 0, 2, 250),
(12, 133, 0, 0, 2, 100),
(12, 134, 2, 550, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `email_verify`
--

CREATE TABLE IF NOT EXISTS `email_verify` (
  `id` int(50) NOT NULL AUTO_INCREMENT,
  `user_id` int(50) NOT NULL,
  `token` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `email_verify`
--

INSERT INTO `email_verify` (`id`, `user_id`, `token`) VALUES
(1, 1, '59422893b038a'),
(2, 2, '59422966efbc5'),
(3, 3, '5942299021052'),
(4, 4, '5944c39540c39');

-- --------------------------------------------------------

--
-- Table structure for table `options`
--

CREATE TABLE IF NOT EXISTS `options` (
  `id` int(50) NOT NULL AUTO_INCREMENT,
  `question_id` int(50) NOT NULL,
  `option_text` varchar(4000) NOT NULL,
  `correct` int(1) NOT NULL,
  `option_text_ml` varchar(4000) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=709 ;

--
-- Dumping data for table `options`
--

INSERT INTO `options` (`id`, `question_id`, `option_text`, `correct`, `option_text_ml`) VALUES
(132, 33, 'Engineering', 1, ''),
(133, 0, 'Leah', 0, 'sebm'),
(134, 0, 'Tamar', 0, 'XmamÀ'),
(135, 0, 'Rachel', 0, 'dmtlÂ'),
(136, 0, 'Zipporah', 1, 'knt¸md'),
(137, 0, 'Jesus', 0, 'tbip'),
(138, 0, 'Peter', 0, ']t{Xmkv'),
(139, 0, 'Zechariah', 0, 'kJdnb'),
(140, 0, 'John', 1, 'tbml¶m³'),
(168, 0, 'Zipporah', 1, 'knt¸md'),
(165, 0, 'Leah', 0, 'sebm'),
(166, 0, 'Tamar', 0, 'XmamÀ'),
(167, 0, 'Rachel', 0, 'dmtlÂ'),
(164, 0, 'Zipporah', 1, 'knt¸md'),
(153, 0, 'Leah', 0, 'sebm'),
(154, 0, 'Tamar', 0, 'XmamÀ'),
(155, 0, 'Rachel', 0, 'dmtlÂ'),
(156, 0, 'Zipporah', 1, 'knt¸md'),
(157, 0, 'Jesus', 0, 'tbip'),
(158, 0, 'Peter', 0, ']t{Xmkv'),
(159, 0, 'Zechariah', 0, 'kJdnb'),
(160, 0, 'John', 1, 'tbml¶m³'),
(161, 0, 'Leah', 0, 'sebm'),
(162, 0, 'Tamar', 0, 'XmamÀ'),
(163, 0, 'Rachel', 0, 'dmtlÂ'),
(131, 33, 'Maths', 0, ''),
(130, 33, 'Science', 0, ''),
(129, 33, 'English', 0, ''),
(128, 32, 'Blah', 0, ''),
(127, 32, 'Mathew', 0, ''),
(126, 32, 'Aasim', 0, ''),
(125, 32, 'Sheryl', 1, ''),
(335, 72, 'Carbon dioxide', 0, ''),
(336, 72, 'Nitrogen', 0, ''),
(189, 0, 'Leah', 0, 'sebm'),
(190, 0, 'Tamar', 0, 'XmamÀ'),
(191, 0, 'Rachel', 0, 'dmtlÂ'),
(192, 0, 'Zipporah', 1, 'knt¸md'),
(334, 72, 'Hydrogen sulphide', 0, ''),
(333, 72, 'Oxygen', 1, ''),
(332, 71, '4', 1, ''),
(331, 71, '3', 0, ''),
(330, 71, '2', 0, ''),
(329, 71, '1', 0, ''),
(328, 70, '52', 0, ''),
(327, 70, '5', 0, ''),
(326, 70, '8', 1, ''),
(325, 70, '7', 0, ''),
(324, 69, '0', 0, ''),
(323, 69, '1', 0, ''),
(320, 68, '12', 0, ''),
(321, 69, '4', 1, ''),
(322, 69, '2', 0, ''),
(317, 68, '9', 1, ''),
(318, 68, '11', 0, ''),
(319, 68, '7', 0, ''),
(316, 67, 'he looked toward heaven and prayed', 0, 'kzÃ€KÂ¯nteÃ§ IÂ®pIÃ„ DbÃ€Â¯n {]mÃ€Â°nÂ¨p'),
(315, 67, 'threw a piece of wood into water ', 1, 'XSnÂ¡jWw shÃ…Â¯nenÂ«p'),
(314, 67, 'raised the staff ', 0, 'hSn DbÃ€Â¯n '),
(313, 67, 'raised his hands and prayed ', 0, 'IcÂ§Ã„ DbÃ€Â¯n {]mÃ€Â°nÂ¨p'),
(312, 66, 'Irenaeus', 0, 'CctWhqkv'),
(311, 66, 'Augustine', 0, 'AKÃŒnÂ³'),
(310, 66, 'Origen', 1, 'HcnPÂ³'),
(309, 66, 'Chrysostom', 0, '{IntkmÃŒw'),
(308, 65, '"Sing to the Lord, for he has triumphed gloriously; horse and rider he has thrown into the sea', 1, 'â€œ IÃ€Â¯mhns ]mSn kvXpXnÃ§hnÂ³; FsÂ´Â¶mÃ‚ AhnSÃ¬ alXz]qÃ€Ã†amb hnPbw tSnbncnÃ§Ã¬. Ã¦Xncsbbpw Ã¦XncÂ¡mcspw AhnSÃ¬ IsensednÂªp'),
(307, 65, '"Lord, your right hand has become glorious in might; Your right hand has scattered the enemies', 0, ' IÃ€Â¯mth, AÂ§bpsS heÂ¯pssI iÃ nbmÃ‚ alXzamÃ€Â¶ncnÃ§Ã¬; IÃ€Â¯mth, AÂ§bpsS heÂ¯pssI i{Xphns NnXdnÂ¨ncnÃ§Ã¬'),
(306, 65, '"In majestic triumph you overthrow your enemies. Your anger blazes out and burns them up like straw', 0, ' AÂ´alnabmÃ‚ AÂ§v FXncmfnIsf XIÃ€Ã§Ã¬; tIm]mÃ¡n AbÂ¨v hbvtÂ¡mens	Â¸mse Ahsc ZlnÂ¸nÃ§Ã¬'),
(241, 0, 'Because it is about God ', 0, 'ssZhs¯çdn¨pffhbmIbmÂ'),
(242, 0, 'Because it came by men and women moved by the Holy Spirit spoke from God', 1, ']cnip²mßmhnmÂ {]tNmZnXcmbhÀ kwkmcn¨hbmIbmÂ'),
(243, 0, 'Because Christ has given the power to the Church', 0, 'k`ímWv tbip A[nImcw ÂInbncnç¶sX¶XpsImïv'),
(244, 0, 'Because there are not people who learn the scriptures ', 0, 'hn.enJnX§Ä ]Tnç¶hÀ CÃm¯XpsImïv'),
(305, 65, '"The Lord is a warrior, the Lord is his name', 0, 'â€œIÃ€Â¯mhv tbmÂ²mhmÃ¦Ã¬; IÃ€Â¯mhv FÂ¶mÃ¦Ã¬ AhnSsÂ¯ maw'),
(302, 64, 'of the poor', 0, 'Zcn{ZcpsS'),
(303, 64, 'of the resident aliens', 0, ']ctZinIfpsS'),
(304, 64, 'of the slaves', 0, 'ASnaIfpsS'),
(298, 63, 'Peter', 0, ']t{Xmkv'),
(299, 63, ']t{Xmkv', 0, 'kJdnb'),
(300, 63, 'John', 1, 'tbmlÂ¶mÂ³'),
(301, 64, 'of widows and orphans', 1, 'hn[hIfpsSbpw AmYcpsSbpw'),
(291, 61, '', 1, ''),
(292, 61, '', 0, ''),
(293, 62, '', 0, ''),
(294, 62, '', 0, ''),
(295, 62, '', 1, ''),
(296, 62, '', 0, ''),
(297, 63, 'Jesus', 0, 'tbip'),
(290, 61, '', 0, ''),
(288, 60, 'Blessed are you among women, and blessed is the fruit of your womb ', 0, 'o kv{XoIfnÃ‚ AÃ«{KloXbmWv. \nsÂ³d DZc^eh AÃ«{KloXw'),
(289, 61, '', 0, ''),
(286, 60, 'The Lord will give him the throne of his ancestor Abraham.', 0, 'AhsÂ³d ]nXmhmb A{_mlÂ¯nsÂ³d knwlmkw ssZhamb IÃ€Â¯mhv Ahv sImSpÃ§w'),
(287, 60, 'You have found favour with God ', 1, 'ssZhkÂ¶n[nbnÃ‚ o Ir] IsÃ¯Â¯nbncnÃ§Ã¬'),
(284, 0, 'John', 1, 'tbmlÂ¶mÂ³'),
(285, 60, 'He will be great before the Lord.', 0, 'IÃ€Â¯mhnsÂ³d kÂ¶n[nbnÃ‚ AhÂ³ henbhmbncnÃ§w'),
(283, 0, 'Zechariah', 0, 'kJdnb'),
(281, 0, 'Jesus', 0, 'tbip'),
(282, 0, 'Peter', 0, ']t{Xmkv'),
(279, 59, 'Rachel', 0, 'dmtlÃ‚'),
(280, 59, 'Zipporah', 1, 'kntÂ¸md'),
(277, 59, 'Leah', 0, 'sebm'),
(278, 59, 'Tamar', 0, 'XmamÃ€'),
(276, 0, 'John', 1, 'tbmlÂ¶mÂ³'),
(274, 0, 'Peter', 0, ']t{Xmkv'),
(275, 0, 'Zechariah', 0, 'kJdnb'),
(273, 0, 'Jesus', 0, 'tbip'),
(261, 0, 'Jesus', 0, 'tbip'),
(262, 0, 'Peter', 0, ']t{Xmkv'),
(263, 0, 'Zechariah', 0, 'kJdnb'),
(264, 0, 'John', 1, 'tbml¶m³'),
(272, 0, 'Zipporah', 1, 'kntÂ¸md'),
(270, 0, 'Tamar', 0, 'XmamÃ€'),
(271, 0, 'Rachel', 0, 'dmtlÃ‚'),
(269, 0, 'Leah', 0, 'sebm'),
(389, 0, 'Because it is about God ', 0, 'ssZhs¯çdn¨pffhbmIbmÂ'),
(390, 0, 'Because it came by men and women moved by the Holy Spirit spoke from God', 1, ']cnip²mßmhnmÂ {]tNmZnXcmbhÀ kwkmcn¨hbmIbmÂ'),
(391, 0, 'Because Christ has given the power to the Church', 0, 'k`ímWv tbip A[nImcw ÂInbncnç¶sX¶XpsImïv'),
(392, 0, 'Because there are not people who learn the scriptures ', 0, 'hn.enJnX§Ä ]Tnç¶hÀ CÃm¯XpsImïv'),
(393, 0, 'Because it is about God ', 0, 'ssZhs¯çdn¨pffhbmIbmÂ'),
(394, 0, 'Because it came by men and women moved by the Holy Spirit spoke from God', 1, ']cnip²mßmhnmÂ {]tNmZnXcmbhÀ kwkmcn¨hbmIbmÂ'),
(395, 0, 'Because Christ has given the power to the Church', 0, 'k`ímWv tbip A[nImcw ÂInbncnç¶sX¶XpsImïv'),
(396, 0, 'Because there are not people who learn the scriptures ', 0, 'hn.enJnX§Ä ]Tnç¶hÀ CÃm¯XpsImïv'),
(397, 0, 'Because it is about God ', 0, 'ssZhs¯çdn¨pffhbmIbmÂ'),
(398, 0, 'Because it came by men and women moved by the Holy Spirit spoke from God', 1, ']cnip²mßmhnmÂ {]tNmZnXcmbhÀ kwkmcn¨hbmIbmÂ'),
(399, 0, 'Because Christ has given the power to the Church', 0, 'k`ímWv tbip A[nImcw ÂInbncnç¶sX¶XpsImïv'),
(400, 0, 'Because there are not people who learn the scriptures ', 0, 'hn.enJnX§Ä ]Tnç¶hÀ CÃm¯XpsImïv'),
(401, 0, 'Because it is about God ', 0, 'ssZhs¯çdn¨pffhbmIbmÂ'),
(402, 0, 'Because it came by men and women moved by the Holy Spirit spoke from God', 1, ']cnip²mßmhnmÂ {]tNmZnXcmbhÀ kwkmcn¨hbmIbmÂ'),
(403, 0, 'Because Christ has given the power to the Church', 0, 'k`ímWv tbip A[nImcw ÂInbncnç¶sX¶XpsImïv'),
(404, 0, 'Because there are not people who learn the scriptures ', 0, 'hn.enJnX§Ä ]Tnç¶hÀ CÃm¯XpsImïv'),
(405, 0, 'Leah', 0, 'sebm'),
(406, 0, 'Tamar', 0, 'XmamÀ'),
(407, 0, 'Rachel', 0, 'dmtlÂ'),
(408, 0, 'Zipporah', 1, 'knt¸md'),
(413, 0, 'Because it is about God ', 0, 'ssZhs¯çdn¨pffhbmIbmÂ'),
(414, 0, 'Because it came by men and women moved by the Holy Spirit spoke from God', 1, ']cnip²mßmhnmÂ {]tNmZnXcmbhÀ kwkmcn¨hbmIbmÂ'),
(415, 0, 'Because Christ has given the power to the Church', 0, 'k`ímWv tbip A[nImcw ÂInbncnç¶sX¶XpsImïv'),
(416, 0, 'Because there are not people who learn the scriptures ', 0, 'hn.enJnX§Ä ]Tnç¶hÀ CÃm¯XpsImïv'),
(425, 0, 'Leah', 0, 'sebm'),
(426, 0, 'Tamar', 0, 'XmamÀ'),
(427, 0, 'Rachel', 0, 'dmtlÂ'),
(428, 0, 'Zipporah', 1, 'knt¸md'),
(429, 0, 'Jesus', 0, 'tbip'),
(430, 0, 'Peter', 0, ']t{Xmkv'),
(431, 0, 'Zechariah', 0, 'kJdnb'),
(432, 0, 'John', 1, 'tbml¶m³'),
(441, 0, 'Because it is about God ', 0, 'ssZhs¯çdn¨pffhbmIbmÂ'),
(442, 0, 'Because it came by men and women moved by the Holy Spirit spoke from God', 1, ']cnip²mßmhnmÂ {]tNmZnXcmbhÀ kwkmcn¨hbmIbmÂ'),
(443, 0, 'Because Christ has given the power to the Church', 0, 'k`ímWv tbip A[nImcw ÂInbncnç¶sX¶XpsImïv'),
(444, 0, 'Because there are not people who learn the scriptures ', 0, 'hn.enJnX§Ä ]Tnç¶hÀ CÃm¯XpsImïv'),
(445, 0, 'Jesus', 0, 'tbip'),
(446, 0, 'Peter', 0, ']t{Xmkv'),
(447, 0, 'Zechariah', 0, 'kJdnb'),
(448, 0, 'John', 1, 'tbml¶m³'),
(449, 0, 'Leah', 0, 'sebm'),
(450, 0, 'Tamar', 0, 'XmamÀ'),
(451, 0, 'Rachel', 0, 'dmtlÂ'),
(452, 0, 'Zipporah', 1, 'knt¸md'),
(508, 0, 'Because there are not people who learn the scriptures ', 0, 'hn.enJnXÂ§Ã„ ]TnÃ§Â¶hÃ€ CÃƒmÂ¯XpsImÃ¯v'),
(507, 0, 'Because Christ has given the power to the Church', 0, 'k`Ã­mWv tbip A[nImcw Ã‚InbncnÃ§Â¶sXÂ¶XpsImÃ¯v'),
(505, 0, 'Because it is about God ', 0, 'ssZhsÂ¯Ã§dnÂ¨pffhbmIbmÃ‚'),
(506, 0, 'Because it came by men and women moved by the Holy Spirit spoke from God', 1, ']cnipÂ²mÃŸmhnmÃ‚ {]tNmZnXcmbhÃ€ kwkmcnÂ¨hbmIbmÃ‚'),
(480, 0, 'Because there are not people who learn the scriptures ', 0, 'hn.enJnXÂ§Ã„ ]TnÃ§Â¶hÃ€ CÃƒmÂ¯XpsImÃ¯v'),
(479, 0, 'Because Christ has given the power to the Church', 0, 'k`Ã­mWv tbip A[nImcw Ã‚InbncnÃ§Â¶sXÂ¶XpsImÃ¯v'),
(478, 0, 'Because it came by men and women moved by the Holy Spirit spoke from God', 1, ']cnipÂ²mÃŸmhnmÃ‚ {]tNmZnXcmbhÃ€ kwkmcnÂ¨hbmIbmÃ‚'),
(477, 0, 'Because it is about God ', 0, 'ssZhsÂ¯Ã§dnÂ¨pffhbmIbmÃ‚'),
(473, 0, 'Leah', 0, 'sebm'),
(474, 0, 'Tamar', 0, 'XmamÀ'),
(475, 0, 'Rachel', 0, 'dmtlÂ'),
(476, 0, 'Zipporah', 1, 'knt¸md'),
(485, 0, 'Leah', 0, 'sebm'),
(486, 0, 'Tamar', 0, 'XmamÃ€'),
(487, 0, 'Rachel', 0, 'dmtlÃ‚'),
(488, 0, 'Zipporah', 1, 'kntÂ¸md'),
(493, 0, 'Jesus', 0, 'tbip'),
(494, 0, 'Peter', 0, ']t{Xmkv'),
(495, 0, 'Zechariah', 0, 'kJdnb'),
(496, 0, 'John', 1, 'tbmlÂ¶mÂ³'),
(509, 100, 'hn.enJnXÂ¯nse {]hNÂ§Ã„ HÃ¬w XsÂ¶ BÃªtSbpw kzÂ´amb hymJymÂ¯nÃ«ffXÃƒ Ahsb hymJym\nÃ§Â¶XnÃ«ff A[nImcw k`Ã­pffXmsWÂ¶Xnv ]t{Xmkv Ã‡olm Ã‚IpÂ¶ ImcWsaÂ´v?', 0, 'ssZhsÂ¯Ã§dnÂ¨pffhbmIbmÃ‚'),
(510, 100, 'Because it came by men and women moved by the Holy Spirit spoke from God', 1, ']cnipÂ²mÃŸmhnmÃ‚ {]tNmZnXcmbhÃ€ kwkmcnÂ¨hbmIbmÃ‚'),
(511, 100, 'Because Christ has given the power to the Church', 0, 'k`Ã­mWv tbip A[nImcw Ã‚InbncnÃ§Â¶sXÂ¶XpsImÃ¯v'),
(512, 100, 'Because there are not people who learn the scriptures ', 0, 'hn.enJnXÂ§Ã„ ]TnÃ§Â¶hÃ€ CÃƒmÂ¯XpsImÃ¯v'),
(513, 101, 'Leah', 0, 'sebm'),
(514, 101, 'Tamar', 0, 'XmamÃ€'),
(515, 101, 'Rachel', 0, 'dmtlÃ‚'),
(516, 101, 'Zipporah', 1, 'kntÂ¸md'),
(517, 102, 'Jesus', 0, 'tbip'),
(518, 102, 'Peter', 0, ']t{Xmkv'),
(519, 102, 'Zechariah', 0, 'kJdnb'),
(520, 102, 'John', 1, 'tbmlÂ¶mÂ³'),
(521, 103, 'Leah', 0, 'sebm'),
(522, 103, 'Tamar', 0, 'XmamÃ€'),
(523, 103, 'Rachel', 0, 'dmtlÃ‚'),
(524, 103, 'Zipporah', 1, 'kntÂ¸md'),
(525, 104, 'Jesus', 0, 'tbip'),
(526, 104, 'Peter', 0, ']t{Xmkv'),
(527, 104, 'Zechariah', 0, 'kJdnb'),
(528, 104, 'John', 1, 'tbmlÂ¶mÂ³'),
(529, 105, 'of widows and orphans', 1, 'hn[hIfpsSbpw AmYcpsSbpw'),
(530, 105, 'of the poor', 0, 'Zcn{ZcpsS'),
(531, 105, 'of the resident aliens', 0, ']ctZinIfpsS'),
(532, 105, 'of the slaves', 0, 'ASnaIfpsS'),
(533, 106, '"The Lord is a warrior, the Lord is his name', 0, 'â€œIÃ€Â¯mhv tbmÂ²mhmÃ¦Ã¬; IÃ€Â¯mhv FÂ¶mÃ¦Ã¬ AhnSsÂ¯ maw'),
(534, 106, '"In majestic triumph you overthrow your enemies. Your anger blazes out and burns them up like straw', 0, ' AÂ´alnabmÃ‚ AÂ§v FXncmfnIsf XIÃ€Ã§Ã¬; tIm]mÃ¡n AbÂ¨v hbvtÂ¡mens	Â¸mse Ahsc ZlnÂ¸nÃ§Ã¬'),
(535, 106, '"Lord, your right hand has become glorious in might; Your right hand has scattered the enemies', 0, ' IÃ€Â¯mth, AÂ§bpsS heÂ¯pssI iÃ nbmÃ‚ alXzamÃ€Â¶ncnÃ§Ã¬; IÃ€Â¯mth, AÂ§bpsS heÂ¯pssI i{Xphns NnXdnÂ¨ncnÃ§Ã¬'),
(536, 106, '"Sing to the Lord, for he has triumphed gloriously; horse and rider he has thrown into the sea', 1, 'â€œ IÃ€Â¯mhns ]mSn kvXpXnÃ§hnÂ³; FsÂ´Â¶mÃ‚ AhnSÃ¬ alXz]qÃ€Ã†amb hnPbw tSnbncnÃ§Ã¬. Ã¦Xncsbbpw Ã¦XncÂ¡mcspw AhnSÃ¬ IsensednÂªp'),
(537, 107, 'Chrysostom', 0, '{IntkmÃŒw'),
(538, 107, 'Origen', 1, 'HcnPÂ³'),
(539, 107, 'Augustine', 0, 'AKÃŒnÂ³'),
(540, 107, 'Irenaeus', 0, 'CctWhqkv'),
(541, 108, 'He will be great before the Lord.', 0, 'IÃ€Â¯mhnsÂ³d kÂ¶n[nbnÃ‚ AhÂ³ henbhmbncnÃ§w'),
(542, 108, 'The Lord will give him the throne of his ancestor Abraham.', 0, 'AhsÂ³d ]nXmhmb A{_mlÂ¯nsÂ³d knwlmkw ssZhamb IÃ€Â¯mhv Ahv sImSpÃ§w'),
(543, 108, 'You have found favour with God ', 1, 'ssZhkÂ¶n[nbnÃ‚ o Ir] IsÃ¯Â¯nbncnÃ§Ã¬'),
(544, 108, 'Blessed are you among women, and blessed is the fruit of your womb ', 0, 'o kv{XoIfnÃ‚ AÃ«{KloXbmWv. \nsÂ³d DZc^eh AÃ«{KloXw'),
(545, 109, 'raised his hands and prayed ', 0, 'IcÂ§Ã„ DbÃ€Â¯n {]mÃ€Â°nÂ¨p'),
(546, 109, 'raised the staff ', 0, 'hSn DbÃ€Â¯n '),
(547, 109, 'threw a piece of wood into water ', 1, 'XSnÂ¡jWw shÃ…Â¯nenÂ«p'),
(548, 109, 'he looked toward heaven and prayed', 0, 'kzÃ€KÂ¯nteÃ§ IÂ®pIÃ„ DbÃ€Â¯n {]mÃ€Â°nÂ¨p'),
(549, 110, 'Large number of flies ', 1, 'CuÂ¨IÃ„ hÃ€Â²nÂ¨Xv'),
(550, 110, 'Festering boils ', 0, '{hWÂ§Ã„ _m[nÂ¨Xv'),
(551, 110, 'Attack of locusts ', 0, 'shÂ«pInfnIÃ„ \ndÂªXv'),
(552, 110, 'Plague of gnats ', 0, 't]Â³ s]ÃªInbXv'),
(553, 111, 'The prayer at Gethsemane, resurrection ', 0, 'KZvtka\nbnse {]mÃ€Â° .. DbnÃ€Â¸v'),
(554, 111, 'Wedding at Cana, crucifixion ', 0, 'Immbnse hnhmlw .. IpcninÃ‚ XdÂ¨Xv'),
(555, 111, 'Finding at the temple, finishing his work ', 1, 'tZhmebÂ¯nÃ‚ IsÃ¯Â¯pÂ¶Xv .. ZuXyw ]qÃ€Â¯nbmÃ§Â¶Xv'),
(556, 111, 'Wedding at Cana, resurrection ', 0, 'Immbnse hnhmlw .. DbnÃ€Â¸v'),
(557, 112, 'the staff turned into a snake and got back to the old form later ', 0, 'hSn ]mÂ¼mÃ¦Â¶Xpw ]nÂ¶oSv ]qÃ€Ã†cq]w {]m]nÃ§Â¶Xpw'),
(558, 112, 'the river changed into blood ', 1, '\Zn cÃ ambn amdpÂ¶Xv'),
(559, 112, 'the hand became leprous when it was kept on the chest and got back to its own form later ', 0, 'amdnSÂ¯nÃ‚ hbvÃ§Â¶ ssI Ã¦jvTapÃ…Xmbn ImWsÂ¸SpÂ¶Xpw ]nÂ¶oSv ]qÃ€Ã†Ã˜nXn {]m]nÃ§Â¶Xpw'),
(560, 112, 'the river water changed into blood on the dry ground ', 0, '\ZoPew IcbnÃ‚ cÃ ambn amdpÂ¶Xv'),
(561, 113, '430', 1, '430'),
(562, 113, '431', 0, '431'),
(563, 113, '400', 0, '400'),
(564, 113, '425', 0, '425'),
(565, 114, '19', 0, ''),
(566, 114, '21', 0, ''),
(567, 114, '18', 0, ''),
(568, 114, '20', 1, ''),
(569, 115, 'Isaiah', 0, 'GkÂ¿m'),
(570, 115, 'Deutronomy', 0, '\nbamhÃ€Â¯w'),
(571, 115, 'Genesis', 0, 'Dev]Â¯n'),
(572, 115, 'Psalm', 1, 'kÂ¦oÃ€Â¯w'),
(573, 116, 'Ex 12:2-13 ', 0, ']pd 12:2..13'),
(574, 116, 'Dt 16:5-6 ', 0, '\nb 16:5..6'),
(575, 116, 'Ex 12:21-28', 0, ']pd 12:21..28'),
(576, 116, 'Ex 12:14-20 ', 1, ']pd 12:14..20'),
(577, 117, 'took ashes from the furnace and threw it up in the air', 1, 'NqfbnÃ‚ \nÂ¶v ssI \ndsb NmcsaSpÂ¯v AÂ´coÂ£Â¯ntesÂ¡dnÂªp'),
(578, 117, 'threw the staff down in front of Pharaoh and his servants ', 0, 'hSn ^dthmbpsSbpw tkhIÃªsSbpw apÂ³]nÃ‚ CÂ«p'),
(579, 117, 'stretched out their hands and struck the ground with the staff ', 0, 'hSnsbSpÂ¯v ssI oÂ«n \nesÂ¯ ]qgnbnÃ‚ ASnÂ¨p'),
(580, 117, 'stretched out their hands over the land of Egypt ', 0, 'CuPn]vXnsÃ¢taÃ‚ ssIoÂ«n'),
(581, 118, 'the staff turned into a snake ', 0, 'hSn ]mÂ¼mbn amdnbXv'),
(582, 118, 'water from the rock ', 0, ']mdbnÃ‚\nÃ¬v shÃ…w'),
(583, 118, 'water in the river turned into blood ', 0, '\ZoPew cÃ ambXv'),
(584, 118, 'bread from heaven ', 1, 'BImiÂ¯nÃ‚\nÃ¬w AÂ¸w hÃ€jnÂ¨Xv'),
(585, 119, 'Healing the lepers ', 0, 'IpjvTtcmKnIsf kpJsÂ¸SpÂ¯nbXv'),
(586, 119, 'Healing the man with unclean spirit ', 0, ']nimNp_m[nXs kpJsÂ¸SpÂ¯nbXv'),
(587, 119, 'Healing the blind beggar ', 1, 'AÃ”s kpJsÂ¸SpÂ¯nbXv'),
(588, 119, 'Triumphal entry into Jerusalem ', 0, 'cmPIobPdpktew {]thiw'),
(589, 120, 'dust of the earth changed into gnats and multiplied', 1, 't]Â³ s]ÃªÃ¦Ã¬'),
(590, 120, 'frogs swarmed all over ', 0, 'XhfIÃ„ hym]nÃ§Ã¬'),
(591, 120, 'water changed into blood ', 0, 'Pew cÃ ambn amdpÃ¬'),
(592, 120, 'the stick turned into a snake ', 0, 'hSn kÃ€Â¸ambn amdpÃ¬'),
(593, 121, 'The parable of the Talent', 0, 'XmeÂ´nsÂ³d D]a'),
(594, 121, 'The parable of the ten pounds ', 1, ']Â¯p mWbÂ§fpsS D]a'),
(595, 121, 'The parable of the great dinner ', 0, 'hnÃªÂ¶nsÂ³d D]a'),
(596, 121, 'The parable of the good samaritan', 0, 'Ãƒ kadnbmÂ¡mcsÂ³d D]a'),
(597, 122, 'Because it is about God ', 0, 'ssZhsÂ¯Ã§dnÂ¨pffhbmIbmÃ‚'),
(598, 122, 'Because it came by men and women moved by the Holy Spirit spoke from God', 1, ']cnipÂ²mÃŸmhnmÃ‚ {]tNmZnXcmbhÃ€ kwkmcnÂ¨hbmIbmÃ‚'),
(599, 122, 'Because Christ has given the power to the Church', 0, 'k`Ã­mWv tbip A[nImcw Ã‚InbncnÃ§Â¶sXÂ¶XpsImÃ¯v'),
(600, 122, 'Because there are not people who learn the scriptures ', 0, 'hn.enJnXÂ§Ã„ ]TnÃ§Â¶hÃ€ CÃƒmÂ¯XpsImÃ¯v'),
(601, 123, 'The least among all of you is the greatest ', 1, '\nÂ§fnÃ‚ GÃ¤hpw sNdnbhÂ³ AhmWv GÃ¤hpw henbhÂ³.'),
(602, 123, 'The greatest among you should be the least and one who has authority should be like the servant. ', 0, '\nÂ§fnÃ‚ GÃ¤hpw henbhÂ³ GÃ¤hpw sNdnbhs	Â¸msebpw A[nImcapffhÂ³ ip{iqjIs	Â¸msebpw BbncnÂ¡Ww.'),
(603, 123, 'Whoever becomes like this little child is the greatest in the kingdom ', 0, 'Cu iniphns	Â¸mse kzbw sNdpXmIpÂ¶hmWv kzÃ€Â¤cmPyÂ¯nse GÃ¤hpw henbhÂ³.'),
(604, 123, 'Whoever wants to be the great should become the servant and whoever wants to be first should become the slave', 0, '\nÂ§fnÃ‚ henbhmImÂ³ B{KlnÃ§Â¶hÂ³ \nÂ§fpsS ip{iqjIÃ«w \nÂ§fnÃ‚ HÂ¶mamImÂ³ B{KlnÃ§Â¶hÂ³ \nÂ§fpsS ZmkÃ«ambncnÂ¡Ww.'),
(605, 124, 'when Moses commanded ', 0, 'tami Iev]nÂ¡ptÂ¼mÃ„'),
(606, 124, 'when Moses came out of tent ', 0, 'tami IqSmcÂ¯np shfnbnÃ‚ hcptÂ¼mÃ„'),
(607, 124, 'when the cloud covered the tent ', 0, 'taLw IqSmcsÂ¯ BhcWw sNÂ¿ptÂ¼mÃ„'),
(608, 124, 'when the cloud was taken up from the tent ', 1, 'taLw IqSmcÂ¯nÃ‚\nÂ¶v DbcptÂ¼mÃ„'),
(609, 125, 'He will be great before the Lord.', 0, 'IÃ€Â¯mhnsÂ³d kÂ¶n[nbnÃ‚ AhÂ³ henbhmbncnÃ§w'),
(610, 125, 'The Lord will give him the throne of his ancestor Abraham.', 0, 'AhsÂ³d ]nXmhmb A{_mlÂ¯nsÂ³d knwlmkw ssZhamb IÃ€Â¯mhv Ahv sImSpÃ§w'),
(611, 125, 'You have found favour with God ', 1, 'ssZhkÂ¶n[nbnÃ‚ o Ir] IsÃ¯Â¯nbncnÃ§Ã¬'),
(612, 125, 'Blessed are you among women, and blessed is the fruit of your womb ', 0, 'o kv{XoIfnÃ‚ AÃ«{KloXbmWv. \nsÂ³d DZc^eh AÃ«{KloXw'),
(613, 126, 'raised his hands and prayed ', 0, 'IcÂ§Ã„ DbÃ€Â¯n {]mÃ€Â°nÂ¨p'),
(614, 126, 'raised the staff ', 0, 'hSn DbÃ€Â¯n '),
(615, 126, 'threw a piece of wood into water ', 1, 'XSnÂ¡jWw shÃ…Â¯nenÂ«p'),
(616, 126, 'he looked toward heaven and prayed', 0, 'kzÃ€KÂ¯nteÃ§ IÂ®pIÃ„ DbÃ€Â¯n {]mÃ€Â°nÂ¨p'),
(617, 127, 'Large number of flies ', 1, 'CuÂ¨IÃ„ hÃ€Â²nÂ¨Xv'),
(618, 127, 'Festering boils ', 0, '{hWÂ§Ã„ _m[nÂ¨Xv'),
(619, 127, 'Attack of locusts ', 0, 'shÂ«pInfnIÃ„ \ndÂªXv'),
(620, 127, 'Plague of gnats ', 0, 't]Â³ s]ÃªInbXv'),
(621, 128, 'The prayer at Gethsemane, resurrection ', 0, 'KZvtka\nbnse {]mÃ€Â° .. DbnÃ€Â¸v'),
(622, 128, 'Wedding at Cana, crucifixion ', 0, 'Immbnse hnhmlw .. IpcninÃ‚ XdÂ¨Xv'),
(623, 128, 'Finding at the temple, finishing his work ', 1, 'tZhmebÂ¯nÃ‚ IsÃ¯Â¯pÂ¶Xv .. ZuXyw ]qÃ€Â¯nbmÃ§Â¶Xv'),
(624, 128, 'Wedding at Cana, resurrection ', 0, 'Immbnse hnhmlw .. DbnÃ€Â¸v'),
(625, 129, 'the staff turned into a snake and got back to the old form later ', 0, 'hSn ]mÂ¼mÃ¦Â¶Xpw ]nÂ¶oSv ]qÃ€Ã†cq]w {]m]nÃ§Â¶Xpw'),
(626, 129, 'the river changed into blood ', 1, '\Zn cÃ ambn amdpÂ¶Xv'),
(627, 129, 'the hand became leprous when it was kept on the chest and got back to its own form later ', 0, 'amdnSÂ¯nÃ‚ hbvÃ§Â¶ ssI Ã¦jvTapÃ…Xmbn ImWsÂ¸SpÂ¶Xpw ]nÂ¶oSv ]qÃ€Ã†Ã˜nXn {]m]nÃ§Â¶Xpw'),
(628, 129, 'the river water changed into blood on the dry ground ', 0, '\ZoPew IcbnÃ‚ cÃ ambn amdpÂ¶Xv'),
(629, 130, '430', 1, ''),
(630, 130, '431', 0, ''),
(631, 130, '400', 0, ''),
(632, 130, '425', 0, ''),
(633, 131, '', 0, '19'),
(634, 131, '', 0, '21'),
(635, 131, '', 0, '18'),
(636, 131, '', 1, '20'),
(637, 132, '19:20', 0, ''),
(638, 132, '20:10', 0, ''),
(639, 132, '20:03', 1, ''),
(640, 132, '20:13', 0, ''),
(641, 133, 'Isaiah', 0, 'GkÂ¿m'),
(642, 133, 'Deutronomy', 0, '\nbamhÃ€Â¯w'),
(643, 133, 'Genesis', 0, 'Dev]Â¯n'),
(644, 133, 'Psalm', 1, 'kÂ¦oÃ€Â¯w'),
(645, 134, 'Ex 12:2-13 ', 0, ']pd 12:2..13'),
(646, 134, 'Dt 16:5-6 ', 0, '\nb 16:5..6'),
(647, 134, 'Ex 12:21-28', 0, ']pd 12:21..28'),
(648, 134, 'Ex 12:14-20 ', 1, ']pd 12:14..20'),
(649, 135, 'The first will be the last, and the last will be the first ', 0, 'AtÂ¸mÃ„ apÂ¼Â·mcmÃ¦Â¶ ]nÂ¼Â·mÃªw ]nÂ¼Â·mcmÃ¦Â¶ apÂ¼Â·mÃªw DÃ¯mbncnÃ§w'),
(650, 135, 'If you do not repent, you too will perish like this ', 0, ']ÃmÂ¯]nÃ§Â¶nsÃƒÂ¦nÃ‚ \nÂ§sfÃƒmhÃªw AXpt]mse inÃ§w'),
(651, 135, 'Ought not this woman, a daughter of Abraham whom Satan bound for eighteen long years, be set free from this bondage on the sabbath day?', 1, ']XnsÂ«p hÃ€jw kmÂ¯mÂ³ _Ã”nÂ¨nÂ«nÃªÂ¶hfmb A{_mlÂ¯nsÃ¢ Cu aIsf km_Â¯p Znhkw AgnÂ¨phntSÃ¯XnsÃƒtÂ¶m?'),
(652, 135, 'Go away from me you who practise injustice ', 0, 'AoXn {]hÃ€Â¯nÃ§Â¶ \nÂ§Ã„ FÂ¶nÃ‚ \nÂ¶v AIÃ¬t]mÃ¦hnÂ³.'),
(653, 136, 'Israelites began the Exodus from Egypt ', 1, 'Pw CuPn]vXnÃ‚\nÂ¶p ]pdsÂ¸Â«p'),
(654, 136, 'People of God reached Canaan ', 0, 'Pw ImmÂ³tZiÂ¯v FÂ¯ntÂ¨Ã€Â¶p'),
(655, 136, 'Israelites received the tablets of the covenant ', 0, 'DSÂ¼SnÂ¸{XnI e`nÂ¨p'),
(656, 136, 'Israelites crossed Jordan ', 0, 'tPmÃ€Â±mÂ³ ISÂ¶p'),
(657, 137, 'Because it is about God ', 0, 'ssZhsÂ¯Ã§dnÂ¨pffhbmIbmÃ‚'),
(658, 137, 'Because it came by men and women moved by the Holy Spirit spoke from God', 1, ']cnipÂ²mÃŸmhnmÃ‚ {]tNmZnXcmbhÃ€ kwkmcnÂ¨hbmIbmÃ‚'),
(659, 137, 'Because Christ has given the power to the Church', 0, 'k`Ã­mWv tbip A[nImcw Ã‚InbncnÃ§Â¶sXÂ¶XpsImÃ¯v'),
(660, 137, 'Because there are not people who learn the scriptures ', 0, 'hn.enJnXÂ§Ã„ ]TnÃ§Â¶hÃ€ CÃƒmÂ¯XpsImÃ¯v'),
(661, 138, 'The least among all of you is the greatest ', 1, '\nÂ§fnÃ‚ GÃ¤hpw sNdnbhÂ³ AhmWv GÃ¤hpw henbhÂ³.'),
(662, 138, 'The greatest among you should be the least and one who has authority should be like the servant. ', 0, '\nÂ§fnÃ‚ GÃ¤hpw henbhÂ³ GÃ¤hpw sNdnbhs	Â¸msebpw A[nImcapffhÂ³ ip{iqjIs	Â¸msebpw BbncnÂ¡Ww.'),
(663, 138, 'Whoever becomes like this little child is the greatest in the kingdom ', 0, 'Cu iniphns	Â¸mse kzbw sNdpXmIpÂ¶hmWv kzÃ€Â¤cmPyÂ¯nse GÃ¤hpw henbhÂ³.'),
(664, 138, 'Whoever wants to be the great should become the servant and whoever wants to be first should become the slave', 0, '\nÂ§fnÃ‚ henbhmImÂ³ B{KlnÃ§Â¶hÂ³ \nÂ§fpsS ip{iqjIÃ«w \nÂ§fnÃ‚ HÂ¶mamImÂ³ B{KlnÃ§Â¶hÂ³ \nÂ§fpsS ZmkÃ«ambncnÂ¡Ww.'),
(665, 139, 'when Moses commanded ', 0, 'tami Iev]nÂ¡ptÂ¼mÃ„'),
(666, 139, 'when Moses came out of tent ', 0, 'tami IqSmcÂ¯np shfnbnÃ‚ hcptÂ¼mÃ„'),
(667, 139, 'when the cloud covered the tent ', 0, 'taLw IqSmcsÂ¯ BhcWw sNÂ¿ptÂ¼mÃ„'),
(668, 139, 'when the cloud was taken up from the tent ', 1, 'taLw IqSmcÂ¯nÃ‚\nÂ¶v DbcptÂ¼mÃ„'),
(669, 140, 'took ashes from the furnace and threw it up in the air', 1, 'NqfbnÃ‚ \nÂ¶v ssI \ndsb NmcsaSpÂ¯v AÂ´coÂ£Â¯ntesÂ¡dnÂªp'),
(670, 140, 'threw the staff down in front of Pharaoh and his servants ', 0, 'hSn ^dthmbpsSbpw tkhIÃªsSbpw apÂ³]nÃ‚ CÂ«p'),
(671, 140, 'stretched out their hands and struck the ground with the staff ', 0, 'hSnsbSpÂ¯v ssI oÂ«n \nesÂ¯ ]qgnbnÃ‚ ASnÂ¨p'),
(672, 140, 'stretched out their hands over the land of Egypt ', 0, 'CuPn]vXnsÃ¢taÃ‚ ssIoÂ«n'),
(673, 141, 'the staff turned into a snake ', 0, 'hSn ]mÂ¼mbn amdnbXv'),
(674, 141, 'water from the rock ', 0, ']mdbnÃ‚\nÃ¬v shÃ…w'),
(675, 141, 'water in the river turned into blood ', 0, '\ZoPew cÃ ambXv'),
(676, 141, 'bread from heaven ', 1, 'BImiÂ¯nÃ‚\nÃ¬w AÂ¸w hÃ€jnÂ¨Xv'),
(677, 142, 'Healing the lepers ', 0, 'IpjvTtcmKnIsf kpJsÂ¸SpÂ¯nbXv'),
(678, 142, 'Healing the man with unclean spirit ', 0, ']nimNp_m[nXs kpJsÂ¸SpÂ¯nbXv'),
(679, 142, 'Healing the blind beggar ', 1, 'AÃ”s kpJsÂ¸SpÂ¯nbXv'),
(680, 142, 'Triumphal entry into Jerusalem ', 0, 'cmPIobPdpktew {]thiw'),
(681, 143, 'dust of the earth changed into gnats and multiplied', 1, 't]Â³ s]ÃªÃ¦Ã¬'),
(682, 143, 'frogs swarmed all over ', 0, 'XhfIÃ„ hym]nÃ§Ã¬'),
(683, 143, 'water changed into blood ', 0, 'Pew cÃ ambn amdpÃ¬'),
(684, 143, 'the stick turned into a snake ', 0, 'hSn kÃ€Â¸ambn amdpÃ¬'),
(685, 144, 'The parable of the Talent', 0, 'XmeÂ´nsÂ³d D]a'),
(686, 144, 'The parable of the ten pounds ', 1, ']Â¯p mWbÂ§fpsS D]a'),
(687, 144, 'The parable of the great dinner ', 0, 'hnÃªÂ¶nsÂ³d D]a'),
(688, 144, 'The parable of the good samaritan', 0, 'Ãƒ kadnbmÂ¡mcsÂ³d D]a'),
(689, 145, 'Leah', 0, 'sebm'),
(690, 145, 'Tamar', 0, 'XmamÃ€'),
(691, 145, 'Rachel', 0, 'dmtlÃ‚'),
(692, 145, 'Zipporah', 1, 'kntÂ¸md'),
(693, 146, 'Jesus', 0, 'tbip'),
(694, 146, 'Peter', 0, ']t{Xmkv'),
(695, 146, 'Zechariah', 0, 'kJdnb'),
(696, 146, 'John', 1, 'tbmlÂ¶mÂ³'),
(697, 147, 'of widows and orphans', 1, 'hn[hIfpsSbpw AmYcpsSbpw'),
(698, 147, 'of the poor', 0, 'Zcn{ZcpsS'),
(699, 147, 'of the resident aliens', 0, ']ctZinIfpsS'),
(700, 147, 'of the slaves', 0, 'ASnaIfpsS'),
(701, 148, '"The Lord is a warrior, the Lord is his name', 0, 'â€œIÃ€Â¯mhv tbmÂ²mhmÃ¦Ã¬; IÃ€Â¯mhv FÂ¶mÃ¦Ã¬ AhnSsÂ¯ maw'),
(702, 148, '"In majestic triumph you overthrow your enemies. Your anger blazes out and burns them up like straw', 0, ' AÂ´alnabmÃ‚ AÂ§v FXncmfnIsf XIÃ€Ã§Ã¬; tIm]mÃ¡n AbÂ¨v hbvtÂ¡mens	Â¸mse Ahsc ZlnÂ¸nÃ§Ã¬'),
(703, 148, '"Lord, your right hand has become glorious in might; Your right hand has scattered the enemies', 0, ' IÃ€Â¯mth, AÂ§bpsS heÂ¯pssI iÃ nbmÃ‚ alXzamÃ€Â¶ncnÃ§Ã¬; IÃ€Â¯mth, AÂ§bpsS heÂ¯pssI i{Xphns NnXdnÂ¨ncnÃ§Ã¬'),
(704, 148, '"Sing to the Lord, for he has triumphed gloriously; horse and rider he has thrown into the sea', 1, 'â€œ IÃ€Â¯mhns ]mSn kvXpXnÃ§hnÂ³; FsÂ´Â¶mÃ‚ AhnSÃ¬ alXz]qÃ€Ã†amb hnPbw tSnbncnÃ§Ã¬. Ã¦Xncsbbpw Ã¦XncÂ¡mcspw AhnSÃ¬ IsensednÂªp'),
(705, 149, 'Chrysostom', 0, '{IntkmÃŒw'),
(706, 149, 'Origen', 1, 'HcnPÂ³'),
(707, 149, 'Augustine', 0, 'AKÃŒnÂ³'),
(708, 149, 'Irenaeus', 0, 'CctWhqkv');

-- --------------------------------------------------------

--
-- Table structure for table `questions`
--

CREATE TABLE IF NOT EXISTS `questions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `quiz_id` int(11) NOT NULL,
  `english` varchar(4000) CHARACTER SET utf8 NOT NULL,
  `malyalam` varchar(4000) CHARACTER SET utf8 NOT NULL,
  `points` int(50) NOT NULL,
  `custom_points` int(10) NOT NULL,
  `category` int(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=150 ;

--
-- Dumping data for table `questions`
--

INSERT INTO `questions` (`id`, `quiz_id`, `english`, `malyalam`, `points`, `custom_points`, `category`) VALUES
(33, 6, 'What are you studying', '', 100, 500, 2),
(32, 6, 'What is your name', '', 100, 200, 1),
(68, 8, '4+5', '', 300, 300, 2),
(69, 8, '2*2', '', 400, 400, 2),
(70, 8, '4+4', '', 500, 500, 2),
(71, 8, '2*2', '', 200, 200, 3),
(72, 9, 'Brass gets discoloured in air because of the presence of which of the following gases in air ', '', 100, 100, 1),
(67, 8, 'What did Moses do to make the water fresh? ', 'amdmbnse Ibv]pÃ… shÃ…w a[pcnÃ§Â¶XnÃ« aptÂ¶mSnbmbn tami FÂ´mWv {]hÃ€Â¯nÂ¨Xv?', 200, 200, 2),
(66, 8, 'The Father of the Church who held that Second Letter of Peter was not written by St. Peter the Apostle. ', 'hnipÂ² ]t{XmknsÃ¢ cÃ¯mw teJw injy{]apJmb ]t{XmkntÃ¢XÃƒ FÂ¶v A`n{]mbsÂ¸Â« k`m]nXmhv ?', 500, 500, 1),
(65, 8, 'Miriam sang to them: complete the sentence: ', 'ancnbmw AhÃ€Â¡v ]mSnsÂ¡mSpÂ¯Xv C{]ImcambnÃªÃ¬:', 400, 400, 1),
(64, 8, 'If you do abuse them, when they cry out to me, I will surely heed their cry". Whose cry? ', '"\nÂ§Ã„ Ahsc D]{ZhnÂ¡pItbm AhÃ€ FsÂ¶ hnfnÂ¡pItbm sNbvXmÃ‚ \nÃbambpw RmÂ³ AhcpsS \nehnfn tIÃ„Â¡pw". BcpsS?', 300, 300, 1),
(60, 8, 'Which of the following did angel Gabriel speak to Mary? ', 'K{_ntbÃ‚ ZqXÂ³ adnbtÂ¯mSp ]dÂª hN\\taXv?', 100, 100, 2),
(61, 8, '', '', 100, 100, 3),
(62, 8, '', '', 100, 100, 4),
(63, 8, 'He must never drink wine or strong drink. About whom is it recorded in Luke''s Gospel?', 'hotÂªm aÃ¤p elcn]m\\obÂ§tfm AhÂ³ IpSnÃ§IbnÃƒ. C{]Imcw eqÂ¡m kphntijÂ¯nÃ‚ tcJsÂ¸SpÂ¯nbncnÃ§Â¶Xv BscÃ§dnÂ¨v.?', 200, 200, 1),
(59, 8, 'What is the name of Moses'' wife??', 'tamibpsS `mcybpsS t]cv??', 100, 100, 1),
(102, 12, 'He must never drink wine or strong drink. About whom is it recorded in Luke''s Gospel?', 'hotÂªm aÃ¤p elcn]m\\obÂ§tfm AhÂ³ IpSnÃ§IbnÃƒ. C{]Imcw eqÂ¡m kphntijÂ¯nÃ‚ tcJsÂ¸SpÂ¯nbncnÃ§Â¶Xv BscÃ§dnÂ¨v.?', 200, 200, 5),
(101, 12, 'What is the name of Moses'' wife??', 'tamibpsS `mcybpsS t]cv??', 100, 100, 5),
(100, 12, 'What is the reason stated by Peter to say that no prophecy of the scripture is a matter of one''s own interpretation, but only the Church has the right to interpret it? ', 'hn.enJnXÂ¯nse {]hN\\Â§Ã„ HÃ¬w XsÂ¶ BÃªtSbpw kzÂ´amb hymJym\\Â¯nÃ«ffXÃƒ Ahsb hymJym\\nÃ§Â¶XnÃ«ff A[nImcw k`Ã­pffXmsWÂ¶Xn\\v ]t{Xmkv Ã‡olm \\Ã‚IpÂ¶ ImcWsaÂ´v?', 300, 300, 3),
(103, 12, 'What is the name of Moses'' wife??', 'tamibpsS `mcybpsS t]cv??', 100, 100, 1),
(104, 12, 'He must never drink wine or strong drink. About whom is it recorded in Luke''s Gospel?', 'hotÂªm aÃ¤p elcn]m\\obÂ§tfm AhÂ³ IpSnÃ§IbnÃƒ. C{]Imcw eqÂ¡m kphntijÂ¯nÃ‚ tcJsÂ¸SpÂ¯nbncnÃ§Â¶Xv BscÃ§dnÂ¨v.?', 200, 200, 1),
(105, 12, 'If you do abuse them, when they cry out to me, I will surely heed their cry". Whose cry? ', '"\\nÂ§Ã„ Ahsc D]{ZhnÂ¡pItbm AhÃ€ FsÂ¶ hnfnÂ¡pItbm sNbvXmÃ‚ \\nÃbambpw RmÂ³ AhcpsS \\nehnfn tIÃ„Â¡pw". BcpsS?', 300, 300, 1),
(106, 12, 'Miriam sang to them: complete the sentence: ', 'ancnbmw AhÃ€Â¡v ]mSnsÂ¡mSpÂ¯Xv C{]ImcambnÃªÃ¬:', 400, 400, 1),
(107, 12, 'The Father of the Church who held that Second Letter of Peter was not written by St. Peter the Apostle. ', 'hnipÂ² ]t{XmknsÃ¢ cÃ¯mw teJ\\w injy{]apJ\\mb ]t{XmkntÃ¢XÃƒ FÂ¶v A`n{]mbsÂ¸Â« k`m]nXmhv ?', 500, 500, 1),
(108, 12, 'Which of the following did angel Gabriel speak to Mary? ', 'K{_ntbÃ‚ ZqXÂ³ adnbtÂ¯mSp ]dÂª hN\\taXv?', 100, 100, 2),
(109, 12, 'What did Moses do to make the water fresh? ', 'amdmbnse Ibv]pÃ… shÃ…w a[pcnÃ§Â¶XnÃ« aptÂ¶mSnbmbn tami FÂ´mWv {]hÃ€Â¯nÂ¨Xv?', 200, 200, 2),
(110, 12, 'What was the punishment that persuaded the Pharaoh to permit the Israelites to offer sacrifices to God within Egypt? ', 'CuPn]vXn\\IÂ¯pXsÂ¶ IÃ€Â¯mhnÃ« _enbÃ€Â¸nÂ¨psImÃ…mÂ³ C{kmtbÃ‚Â¡msc AÃ«hZnÂ¡mÂ³ ^dthmsb t{]cnÂ¸nÂ¨ inÂ£?', 300, 300, 2),
(111, 12, 'What are the two events recorded by Luke from the life of Jesus as taking place after three days?', 'tbiphnsÂ³d PohnXÂ¯nÃ‚ aqÂ¶mw Znhkw \\SÃ§Â¶Xmbn eqÂ¡m tcJsÂ¸SpÂ¯pÂ¶ cÃ¯p kw`hÂ§tfXv?', 400, 400, 2),
(112, 12, 'Which of the following was not the sign given by God to prove the authority of Moses? ', 'tamibpsS B[nImcnIX AwKoIcnÂ¡mÂ³ ssZhw \\Ã‚Inb ASbmfÂ§fnÃ‚ s]SmÂ¯Xv GXv?', 500, 500, 2),
(113, 12, 'How many years did the Israelites live in Egypt? ', 'C{kmtbÃ‚Â¡mÃªsS CuPn]vXnse hmkImew F{X hÃ€jambnÃªÃ¬?', 100, 100, 3),
(114, 12, 'Which is the only chapter in which Sadducees are mentioned? ', 'eqÂ¡mbpsS kphntijÂ¯nÃ‚ kZpÂ¡mbÃ€ {]XyÂ£sÂ¸SpÂ¶ GI AÂ²ymbw?', 200, 200, 3),
(115, 12, 'Where in the Old Testament does the following verse occur "With the Lord one day is like a thousand years and a thousand years like one day? ', 'IÃ€Â¯mhnsÂ³d apÂ¼nÃ‚ HÃª Znhkw Bbncw hÃ€jÂ§Ã„ t]msebpw Bbncw hÃ€jÂ§Ã„ HÃª Znhkw t]msebpamWv. Cu hN\\w ]gb\\nbaÂ¯nse GXp ]pkvXIÂ¯nÃ‚ \\nÂ¶v?', 400, 400, 3),
(116, 12, 'Which are the verses that refer to the Feast of the Unleavened Bread in the Old Testament?', ' ]pfnÂ¸nÃƒmÂ¯ AÂ¸Â¯nsÃ¢ XnÃª\\mfns\\Ã§dnÂ¨pÃ… (22:7) ]gb\\nba`mKw GXv?', 500, 500, 3),
(117, 12, 'What did Moses and Aaron do before bringing festering boils?', 'a\\pjycnepw arKÂ§fnepw s]mÂ«nsbmenÃ§Â¶ {hWÂ§fpÃ¯mÃ¦Â¶Xn\\p aptÂ¶mSnbmbn Altdm\\pw tamibpw FÂ´mWv sNbvXXv?', 100, 100, 4),
(118, 12, 'What was the miracle God performed saying, "I will test them, whether they will follow my instruction or not"?', 'C{kmtbÃ‚Â¡mÃ€ FsÃ¢ \\nbaaÃ«kcnÂ¨v \\SÃ§tam CÃƒtbm FÃ¬ RmÂ³ ]coÂ£nÃ§w FÃ¬ ]dÂªpsImÃ¯v IÃ€Â¯mhp sNbvX AÃ›pXw?', 200, 200, 4),
(119, 12, 'What is the fifth miracle of Jesus on his way to Jerusalem? ', 'PdpkteanteÃ§ff bm{XbnÃ‚ tbip {]hÃ€Â¯nÂ¨ AÂ©masÂ¯ AÃ›pXw.', 300, 300, 4),
(120, 12, 'The Egyptian magicians also performed the same, as Moses and Aaron did, by their magic arts. But they could not do one of the signs given below. Which was that sign? ', 'tamisbbpw Altdms\\bpwt]mse a{Â´hmZnIfpw XÂ§fpsS am{Â´nI hnZybmÃ‚ CuPn]vXnÃ‚ AtX ASbmfÂ§Ã„ {]hÃ€Â¯nÂ¨p. FÂ¶mÃ‚ Xmsg sImSpÂ¯ncnÃ§Â¶ \\mev DÂ¯cÂ§fnÃ‚ HÃª ASbmfw {]hÃ€Â¯nÂ¡mÂ³ a{Â´hmZnIÃ„Â¡v km[nÂ¨nÃƒ. icnbpÂ¯cw XncsÂªSpÃ§I.', 400, 400, 4),
(121, 12, 'Which is the parable in the Gospel of Luke that teaches the lesson that, if Christians do their duty till the second coming of the Lord, they will receive great reward?', 'IÃ€Â¯mhnsÂ³d cÃ¯masÂ¯ BKa\\w hsc {InkvXym\\nIÃ„ {]hÃ€Â¯\\\\ncXcmIWsaÃ¬w AtÂ¸mÃ„ AhÃ€Ã§w IqSpXÃ‚ {]Xn^ew e`nÃ§saÃ¬ ]TnÂ¸nÃ§Â¶ eqÂ¡mkphntijÂ¯nse D]atbXv??', 500, 500, 4),
(122, 12, 'What is the reason stated by Peter to say that no prophecy of the scripture is a matter of one''s own interpretation, but only the Church has the right to interpret it? ', 'hn.enJnXÂ¯nse {]hN\\Â§Ã„ HÃ¬w XsÂ¶ BÃªtSbpw kzÂ´amb hymJym\\Â¯nÃ«ffXÃƒ Ahsb hymJym\\nÃ§Â¶XnÃ«ff A[nImcw k`Ã­pffXmsWÂ¶Xn\\v ]t{Xmkv Ã‡olm \\Ã‚IpÂ¶ ImcWsaÂ´v?', 300, 300, 5),
(123, 12, 'What was the answer given by Jesus over the first dispute of the disciples regarding "who was the greatest among them"?', 'BZy{]mhiyw "XÂ§fnÃ‚ henbhÂ³ BcmWv" FÂ¶Xns\\Â¸Ã¤n injyÃªsS CSbnÃ‚ DÃ¯mb XÃ€Â¡Â¯n\\v tbip \\Ã‚Inb adp]SnsbÂ´v?', 400, 400, 5),
(124, 12, 'When did the people of Israel set out on their journey? ', 'FtÂ¸mgmWv C{kmtbÃ‚P\\w bm{X ]pdsÂ¸Â«ncpÂ¶Xv?', 500, 500, 5),
(125, 13, 'Which of the following did angel Gabriel speak to Mary? ', 'K{_ntbÃ‚ ZqXÂ³ adnbtÂ¯mSp ]dÂª hN\\taXv?', 100, 100, 1),
(126, 13, 'What did Moses do to make the water fresh? ', 'amdmbnse Ibv]pÃ… shÃ…w a[pcnÃ§Â¶XnÃ« aptÂ¶mSnbmbn tami FÂ´mWv {]hÃ€Â¯nÂ¨Xv?', 200, 200, 1),
(127, 13, 'What was the punishment that persuaded the Pharaoh to permit the Israelites to offer sacrifices to God within Egypt? ', 'CuPn]vXn\\IÂ¯pXsÂ¶ IÃ€Â¯mhnÃ« _enbÃ€Â¸nÂ¨psImÃ…mÂ³ C{kmtbÃ‚Â¡msc AÃ«hZnÂ¡mÂ³ ^dthmsb t{]cnÂ¸nÂ¨ inÂ£?', 300, 300, 1),
(128, 13, 'What are the two events recorded by Luke from the life of Jesus as taking place after three days?', 'tbiphnsÂ³d PohnXÂ¯nÃ‚ aqÂ¶mw Znhkw \\SÃ§Â¶Xmbn eqÂ¡m tcJsÂ¸SpÂ¯pÂ¶ cÃ¯p kw`hÂ§tfXv?', 400, 400, 1),
(129, 13, 'Which of the following was not the sign given by God to prove the authority of Moses? ', 'tamibpsS B[nImcnIX AwKoIcnÂ¡mÂ³ ssZhw \\Ã‚Inb ASbmfÂ§fnÃ‚ s]SmÂ¯Xv GXv?', 500, 500, 1),
(130, 13, 'How many years did the Israelites live in Egypt? ', 'C{kmtbÃ‚Â¡mÃªsS CuPn]vXnse hmkImew F{X hÃ€jambnÃªÃ¬?', 100, 100, 2),
(131, 13, 'Which is the only chapter in which Sadducees are mentioned? ', 'eqÂ¡mbpsS kphntijÂ¯nÃ‚ kZpÂ¡mbÃ€ {]XyÂ£sÂ¸SpÂ¶ GI AÂ²ymbw?', 200, 200, 2),
(132, 13, 'You shall not have other Gods besides me. Mention the chapter and verse.?', 'Rm\\ÃƒmsX thsd tZhÂ·mÃ€ \\n\\Â¡v DÃ¯mIÃªXv. GXv A[ymbw? GXv hmIyw?', 300, 300, 2),
(133, 13, 'Where in the Old Testament does the following verse occur "With the Lord one day is like a thousand years and a thousand years like one day? ', 'IÃ€Â¯mhnsÂ³d apÂ¼nÃ‚ HÃª Znhkw Bbncw hÃ€jÂ§Ã„ t]msebpw Bbncw hÃ€jÂ§Ã„ HÃª Znhkw t]msebpamWv. Cu hN\\w ]gb\\nbaÂ¯nse GXp ]pkvXIÂ¯nÃ‚ \\nÂ¶v?', 400, 400, 2),
(134, 13, 'Which are the verses that refer to the Feast of the Unleavened Bread in the Old Testament?', ' ]pfnÂ¸nÃƒmÂ¯ AÂ¸Â¯nsÃ¢ XnÃª\\mfns\\Ã§dnÂ¨pÃ… (22:7) ]gb\\nba`mKw GXv?', 500, 500, 2),
(135, 13, 'Hearing Jesus" words, His opponents were put to shame. What did Jesus say?', 'tbiphnsÃ¢ "{]XntbmKnIÃ„ eÃ–nXcmb"Xv GXp hN\\w tIÂ«mWv? ', 100, 100, 3),
(136, 13, 'What is the speciality of the month of Abibu?? ', 'A_o_pamkÂ¯nsÃ¢ {]tXyIX FÂ´mWv??', 200, 200, 3),
(137, 13, 'What is the reason stated by Peter to say that no prophecy of the scripture is a matter of one''s own interpretation, but only the Church has the right to interpret it? ', 'hn.enJnXÂ¯nse {]hN\\Â§Ã„ HÃ¬w XsÂ¶ BÃªtSbpw kzÂ´amb hymJym\\Â¯nÃ«ffXÃƒ Ahsb hymJym\\nÃ§Â¶XnÃ«ff A[nImcw k`Ã­pffXmsWÂ¶Xn\\v ]t{Xmkv Ã‡olm \\Ã‚IpÂ¶ ImcWsaÂ´v?', 300, 300, 3),
(138, 13, 'What was the answer given by Jesus over the first dispute of the disciples regarding "who was the greatest among them"?', 'BZy{]mhiyw "XÂ§fnÃ‚ henbhÂ³ BcmWv" FÂ¶Xns\\Â¸Ã¤n injyÃªsS CSbnÃ‚ DÃ¯mb XÃ€Â¡Â¯n\\v tbip \\Ã‚Inb adp]SnsbÂ´v?', 400, 400, 3),
(139, 13, 'When did the people of Israel set out on their journey? ', 'FtÂ¸mgmWv C{kmtbÃ‚P\\w bm{X ]pdsÂ¸Â«ncpÂ¶Xv?', 500, 500, 3),
(140, 13, 'What did Moses and Aaron do before bringing festering boils?', 'a\\pjycnepw arKÂ§fnepw s]mÂ«nsbmenÃ§Â¶ {hWÂ§fpÃ¯mÃ¦Â¶Xn\\p aptÂ¶mSnbmbn Altdm\\pw tamibpw FÂ´mWv sNbvXXv?', 100, 100, 4),
(141, 13, 'What was the miracle God performed saying, "I will test them, whether they will follow my instruction or not"?', 'C{kmtbÃ‚Â¡mÃ€ FsÃ¢ \\nbaaÃ«kcnÂ¨v \\SÃ§tam CÃƒtbm FÃ¬ RmÂ³ ]coÂ£nÃ§w FÃ¬ ]dÂªpsImÃ¯v IÃ€Â¯mhp sNbvX AÃ›pXw?', 200, 200, 4),
(142, 13, 'What is the fifth miracle of Jesus on his way to Jerusalem? ', 'PdpkteanteÃ§ff bm{XbnÃ‚ tbip {]hÃ€Â¯nÂ¨ AÂ©masÂ¯ AÃ›pXw.', 300, 300, 4),
(143, 13, 'The Egyptian magicians also performed the same, as Moses and Aaron did, by their magic arts. But they could not do one of the signs given below. Which was that sign? ', 'tamisbbpw Altdms\\bpwt]mse a{Â´hmZnIfpw XÂ§fpsS am{Â´nI hnZybmÃ‚ CuPn]vXnÃ‚ AtX ASbmfÂ§Ã„ {]hÃ€Â¯nÂ¨p. FÂ¶mÃ‚ Xmsg sImSpÂ¯ncnÃ§Â¶ \\mev DÂ¯cÂ§fnÃ‚ HÃª ASbmfw {]hÃ€Â¯nÂ¡mÂ³ a{Â´hmZnIÃ„Â¡v km[nÂ¨nÃƒ. icnbpÂ¯cw XncsÂªSpÃ§I.', 400, 400, 4),
(144, 13, 'Which is the parable in the Gospel of Luke that teaches the lesson that, if Christians do their duty till the second coming of the Lord, they will receive great reward?', 'IÃ€Â¯mhnsÂ³d cÃ¯masÂ¯ BKa\\w hsc {InkvXym\\nIÃ„ {]hÃ€Â¯\\\\ncXcmIWsaÃ¬w AtÂ¸mÃ„ AhÃ€Ã§w IqSpXÃ‚ {]Xn^ew e`nÃ§saÃ¬ ]TnÂ¸nÃ§Â¶ eqÂ¡mkphntijÂ¯nse D]atbXv??', 500, 500, 4),
(145, 13, 'What is the name of Moses'' wife??', 'tamibpsS `mcybpsS t]cv??', 100, 100, 5),
(146, 13, 'He must never drink wine or strong drink. About whom is it recorded in Luke''s Gospel?', 'hotÂªm aÃ¤p elcn]m\\obÂ§tfm AhÂ³ IpSnÃ§IbnÃƒ. C{]Imcw eqÂ¡m kphntijÂ¯nÃ‚ tcJsÂ¸SpÂ¯nbncnÃ§Â¶Xv BscÃ§dnÂ¨v.?', 200, 200, 5),
(147, 13, 'If you do abuse them, when they cry out to me, I will surely heed their cry". Whose cry? ', '"\\nÂ§Ã„ Ahsc D]{ZhnÂ¡pItbm AhÃ€ FsÂ¶ hnfnÂ¡pItbm sNbvXmÃ‚ \\nÃbambpw RmÂ³ AhcpsS \\nehnfn tIÃ„Â¡pw". BcpsS?', 300, 300, 5),
(148, 13, 'Miriam sang to them: complete the sentence: ', 'ancnbmw AhÃ€Â¡v ]mSnsÂ¡mSpÂ¯Xv C{]ImcambnÃªÃ¬:', 400, 400, 5),
(149, 13, 'The Father of the Church who held that Second Letter of Peter was not written by St. Peter the Apostle. ', 'hnipÂ² ]t{XmknsÃ¢ cÃ¯mw teJ\\w injy{]apJ\\mb ]t{XmkntÃ¢XÃƒ FÂ¶v A`n{]mbsÂ¸Â« k`m]nXmhv ?', 500, 500, 5);

-- --------------------------------------------------------

--
-- Table structure for table `quiz`
--

CREATE TABLE IF NOT EXISTS `quiz` (
  `id` int(50) NOT NULL AUTO_INCREMENT,
  `start_time` date NOT NULL,
  `end_time` date NOT NULL,
  `title` varchar(100) NOT NULL,
  `ongoing_flag` int(11) NOT NULL DEFAULT '0',
  `curr_chance` int(10) NOT NULL,
  `c1` varchar(100) NOT NULL,
  `c2` varchar(100) NOT NULL,
  `c3` varchar(100) NOT NULL,
  `c4` varchar(100) NOT NULL,
  `c5` varchar(100) NOT NULL,
  `doubleflag` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `quiz`
--

INSERT INTO `quiz` (`id`, `start_time`, `end_time`, `title`, `ongoing_flag`, `curr_chance`, `c1`, `c2`, `c3`, `c4`, `c5`, `doubleflag`) VALUES
(9, '0000-00-00', '0000-00-00', 'Test Quiz', 1, 127, 'Science ', 'Maths ', 'History', 'Geography', 'Environment', 1),
(12, '0000-00-00', '0000-00-00', 'Mattest', 1, 132, 'Category 1', 'Category 2', 'Category 3', 'Category 4', 'Category 5', 1),
(6, '0000-00-00', '0000-00-00', 'Church', 1, 95, 'Category 1', 'Category 2', 'Category 3', 'Category 4', 'Category 5', 0),
(8, '0000-00-00', '0000-00-00', 'Vichara', 1, 108, 'Category 1', 'Category 2', 'Category 3', 'Category 4', 'Category 5', 0),
(13, '0000-00-00', '0000-00-00', 'Jan31-2018', 1, 117, 'ACTIONS', 'NUMBERS', 'GENERAL', 'PARABLES & MIRACLES', 'NAMES', 0),
(14, '0000-00-00', '0000-00-00', 'TEST', 1, 122, 'a', 'B', 'C', 'Category 4', 'Category 5', 0);

-- --------------------------------------------------------

--
-- Table structure for table `quiz_answers`
--

CREATE TABLE IF NOT EXISTS `quiz_answers` (
  `id` int(50) NOT NULL AUTO_INCREMENT,
  `user_id` int(50) NOT NULL,
  `questions_id` int(50) NOT NULL,
  `quiz_id` int(50) NOT NULL,
  `points` decimal(50,0) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=465 ;

--
-- Dumping data for table `quiz_answers`
--

INSERT INTO `quiz_answers` (`id`, `user_id`, `questions_id`, `quiz_id`, `points`) VALUES
(336, 97, 32, 6, '100'),
(335, 97, 32, 6, '100'),
(334, 96, 32, 6, '100'),
(333, 98, 32, 6, '0'),
(332, 95, 32, 6, '100'),
(331, 97, 33, 6, '500'),
(411, 119, 142, 13, '0'),
(410, 118, 142, 13, '150'),
(409, 117, 142, 13, '150'),
(408, 118, 142, 13, '150'),
(167, 0, 0, 0, '100'),
(166, 0, 0, 0, '10'),
(165, 0, 0, 0, '10'),
(407, 117, 142, 13, '150'),
(406, 118, 142, 13, '150'),
(405, 119, 142, 13, '0'),
(404, 119, 142, 13, '0'),
(403, 117, 142, 13, '150'),
(402, 121, 143, 13, '200'),
(401, 118, 143, 13, '0'),
(400, 120, 143, 13, '200'),
(399, 119, 143, 13, '200'),
(398, 117, 143, 13, '200'),
(397, 118, 144, 13, '250'),
(396, 117, 144, 13, '0'),
(395, 119, 144, 13, '250'),
(394, 121, 145, 13, '100'),
(393, 120, 146, 13, '200'),
(392, 119, 147, 13, '300'),
(391, 118, 148, 13, '600'),
(390, 117, 149, 13, '1000'),
(458, 131, 116, 12, '250'),
(457, 130, 116, 12, '250'),
(456, 132, 121, 12, '1000'),
(455, 131, 124, 12, '0'),
(337, 98, 32, 6, '0'),
(338, 95, 32, 6, '100'),
(339, 96, 32, 6, '100'),
(454, 134, 124, 12, '250'),
(384, 107, 69, 8, '400'),
(383, 110, 62, 8, '0'),
(382, 108, 61, 8, '50'),
(381, 109, 61, 8, '0'),
(380, 109, 61, 8, '0'),
(379, 108, 61, 8, '50'),
(378, 108, 60, 8, '100'),
(377, 107, 59, 8, '100'),
(412, 120, 141, 13, '200'),
(413, 121, 140, 13, '400'),
(414, 117, 139, 13, '1000'),
(415, 118, 138, 13, '1700'),
(416, 117, 137, 13, '150'),
(417, 118, 137, 13, '150'),
(418, 119, 137, 13, '-300'),
(419, 121, 136, 13, '100'),
(420, 120, 136, 13, '-300'),
(421, 119, 136, 13, '100'),
(422, 117, 135, 13, '50'),
(423, 119, 135, 13, '50'),
(424, 121, 135, 13, '0'),
(425, 117, 130, 13, '0'),
(426, 118, 131, 13, '200'),
(427, 120, 134, 13, '250'),
(428, 119, 134, 13, '-600'),
(429, 121, 134, 13, '250'),
(430, 120, 133, 13, '650'),
(431, 121, 132, 13, '1350'),
(432, 119, 129, 13, '250'),
(433, 120, 129, 13, '250'),
(434, 117, 129, 13, '0'),
(435, 119, 128, 13, '200'),
(436, 118, 128, 13, '0'),
(437, 120, 128, 13, '200'),
(438, 119, 127, 13, '300'),
(439, 119, 126, 13, '100'),
(440, 120, 126, 13, '0'),
(441, 121, 125, 13, '100'),
(453, 130, 124, 12, '250'),
(452, 132, 124, 12, '250'),
(451, 130, 108, 12, '100'),
(450, 134, 100, 12, '300'),
(449, 133, 103, 12, '100'),
(448, 126, 72, 9, '100'),
(459, 133, 116, 12, '-100'),
(460, 134, 112, 12, '1050'),
(461, 131, 107, 12, '250'),
(462, 133, 107, 12, '250'),
(463, 130, 107, 12, '-600'),
(464, 131, 117, 12, '100');

-- --------------------------------------------------------

--
-- Table structure for table `quiz_participants`
--

CREATE TABLE IF NOT EXISTS `quiz_participants` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `quiz_id` int(10) NOT NULL,
  `team_name` varchar(1000) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=135 ;

--
-- Dumping data for table `quiz_participants`
--

INSERT INTO `quiz_participants` (`id`, `quiz_id`, `team_name`) VALUES
(97, 6, 'dad'),
(96, 6, 'admin2'),
(95, 6, 'admin'),
(110, 8, 'blah 4'),
(109, 8, 'blah 3'),
(108, 8, 'blah 2'),
(107, 8, 'blah'),
(98, 6, 'awdawd'),
(130, 12, 'A'),
(129, 9, 'E'),
(128, 9, 'D'),
(127, 9, 'C'),
(126, 9, 'B'),
(125, 9, 'A'),
(117, 13, 'Mathew Abraham'),
(118, 13, 'Saly Abraham'),
(119, 13, 'Albert Abraham'),
(120, 13, 'Andrew Abraham'),
(121, 13, 'Austin Abraham'),
(122, 14, 'A'),
(123, 14, 'b'),
(124, 14, 'c'),
(131, 12, 'B'),
(132, 12, 'C'),
(133, 12, 'D'),
(134, 12, 'E');

-- --------------------------------------------------------

--
-- Table structure for table `reset_password`
--

CREATE TABLE IF NOT EXISTS `reset_password` (
  `id` int(50) NOT NULL AUTO_INCREMENT,
  `user_id` int(50) NOT NULL,
  `token` varchar(100) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `password` varchar(100) NOT NULL,
  `email` varchar(80) NOT NULL,
  `contact` int(10) NOT NULL,
  `type` int(1) NOT NULL,
  `verified` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `email`, `contact`, `type`, `verified`) VALUES
(1, 'aasimkhan30', '$2y$12$9tzHUHwCLTg4SpZxHUXSz.KGgTmdSLEvgGA1d3KoohiC800W4tB.a', 'aasimkhan30@gmail.com', 0, 0, 1),
(2, 'aasimkhan301', '$2y$12$GTYiuj1mCOSeXBz61hIov.zrIGzaFY1iMrSDAzdflA57m313czNgy', 'aasim.khan30@gmail.com', 0, 0, 1),
(3, 'aasimkhan302', '$2y$12$a.dTT9h1kF90uGQUSsmuwelW5gUXOSgDvzagjikcZnHyFEx7zuLM6', 'aa.simkhan30@gmail.com', 0, 0, 1),
(4, 'aasimkhan303', '$2y$12$1ytXelSCays99ljrmLE4Ze2EQtTZANIL0k0DQ.ZngL6OvsSw9jYmG', 'aasimkhan3045@gmail.com', 0, 0, 1);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
